export class Observable<T = any>{
	constructor(private callback: (observer: Observer<T>) => any = () => {}){ }

	subscribe(observer: Observer<T>) {
		if(!observer.next) {
			observer.complete = () => {};
		}
		if(!observer.error) {
			observer.complete = () => {};
		}
		if(!observer.complete) {
			observer.complete = () => {};
		}
		this.callback(observer);	
	}
}

export interface Observer<T> {
	next?(value: T),
	error?(),
	complete?(),
}

let myObservable = new Observable<number>((observer)=>{
	observer.next(1);
	observer.next(2);
	observer.next(3);
	setTimeout(()=> {
		observer.next(5);
		observer.complete();
	}, 3000);
});

myObservable.subscribe({
	next(val) {
		console.log('A: next value', val);
	}
});

console.log('11');
